/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drivetrain;

import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;

import frc.robot.Robot;

/**
 * Add your docs here.
 */
public class Drive implements Command {

    private Set<Subsystem> requirements;

    public Drive(Subsystem ... s){
        requirements = new HashSet<>();
        for(Subsystem sub: s){
            requirements.add(sub);
        }
        SmartDashboard.putBoolean("create",true);
    }

    public void initialize(){
        SmartDashboard.putBoolean("init",true);
    }

    public Set<Subsystem> getRequirements(){
        return requirements;
    }

    public void execute() {
        SmartDashboard.putBoolean("exec",true);
        Robot.drivetrain.drive(Robot.oi.getLeftJoystickX(), Robot.oi.getLeftJoystickY());
    }

    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void end(boolean f){
        if(f){
            SmartDashboard.putBoolean("interrupt",true);
        }else{
            SmartDashboard.putBoolean("end",true);
        }
    }
    
}
